import java.io.FileNotFoundException;


public class Driver
{

  public static void main(String[] args) throws FileNotFoundException
  {
    MethodPractice util = new MethodPractice();

    int[] intArray1 =
    {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    /////////////// Switches ///////////////
    boolean rotateLeftBy = false;
    boolean onlyOddNumbers = false;
    boolean fileCleaner = false;
    boolean maxDifference = true;
    ////////////////////////////////////////

    // rotateLeftBy
    if (rotateLeftBy == true)
    {
      util.rotateLeftBy(intArray1, 2);
      System.out.println(util.displayArray(intArray1));
    }

    int[] intArray2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    // onlyOddNumbers
    if (onlyOddNumbers == true)
    {
      util.onlyOddNumbers(intArray2);
      System.out.println(util.displayArray(intArray2));
    }

    // fileCleaner
    if (fileCleaner == true)
    {
      String strInFile = "input.txt";
      String strOutfile = "output.txt";
      util.fileCleaner(strInFile, strOutfile);
    }
    
    int[] intArray3 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    
    // maxDiffernece
    if (maxDifference = true)
    {
      System.out.println(util.maxDistance(intArray3));
    }
  }

}
