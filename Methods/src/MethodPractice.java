import java.util.*;
import java.io.*;


public class MethodPractice
{

  /////////////////////////////////////////////////////////////////////
  // Write a method called "squareDoubleArray" that accepts an array of doubles as
  // its only argument. The method should return an array of doubles that are 
  // squares of the values of the input array. DO NOT overwrite the input array.
  // An example of its use could be:
  //
  // double[] dInput = { 3.0, 1.5, 5.0 };
  // double[] dOutputp = squareDoubleArray(dInput); // 9.0, 2.25, 25.0

  public double[] squareDoubleArray(double[] dNumbers)
  {
    double[] dResults = new double[dNumbers.length];

    for (int index = 0; index < dResults.length; index++)
    {
      dResults[index] = dNumbers[index] * dNumbers[index];
    }

    return dResults;
  }

  /////////////////////////////////////////////////////////////////////
  //  
  //  Write a  method called "rotateLeftBy" that accepts two parameters. The first
  //  parameter is an array of integers. The second parameter must be a positive
  //  integer. The method MODIFIES the input array so that its values are rotated
  //  to the left by the number of positions specified by the first argument. 
  //  An example of its use could be:
  //
  //  int[] values = { 1, 2, 3, 4, 5, 6, 7 };
  //  int shift = 2;
  //
  //  rotateLeftBy(values, shift); // 3, 4, 5, 6, 7, 1, 2

  public void rotateLeftBy(int[] iNumbers, int leftShift)
  {
    for (int shift = 1; shift <= leftShift; shift++)
    {
      int left = iNumbers[0];

      for (int index = 1; index < iNumbers.length; index++)
      {
        iNumbers[index - 1] = iNumbers[index];
      }

      iNumbers[iNumbers.length - 1] = left;
    }
  }

  public String displayArray(int[] iNumbers)
  {
    String str = "";
    for (int index = 0; index < iNumbers.length - 1; index++)
    {
      str += iNumbers[index] + ", ";
    }
    str += iNumbers[iNumbers.length - 1];

    return str;
  }

  ///////////////////////////////////////////////////////////////////
  //
  //  Write a method called "onlyOddNumbers" that accepts an integer array as its only
  //  parameter. The method returns an integer array containing only odd numbers from
  //  the input parameter array. 
  //
  //  int[] values = { 3, 9, 4, 1, 5, 6 };
  //  int[] results;
  //  results = onlyOddNumbers(values); // 3,9,1,5

  public int[] onlyOddNumbers(int[] iNumbers)
  {
    // find how many odds are in the array
    int count = 0;
    for (int index = 0; index < iNumbers.length; index++)
    {
      if ((iNumbers[index] % 2) == 1) //is it odd ?
        count++;
    }

    // find them and store in a new array iResults
    int[] iResults = new int[count];
    iResults = iNumbers;
    int location = 0;
    for (int index = 0; index < iNumbers.length; index++)
    {
      if ((iNumbers[index] % 2) == 1) //is it odd ?
      {
        iResults[location] = iNumbers[index];
        location++;
      }
    }

    return iResults;
  }

  ///////////////////////////////////////////////////////////////////
  //
  //Write a Java method that accepts two parameters. The first parameter is an
  //input file name and the second is the output file name (both Strings). The
  //method should copy the content of the input file into the output file while
  //eliminating all empty lines.

  public void fileCleaner(String inFile, String outFile) throws FileNotFoundException
  {
    Scanner inputFile = new Scanner(new File(inFile));
    PrintWriter outputFile = new PrintWriter(outFile);
    try
    {
    inputFile = new Scanner(new File(inFile));
    outputFile = new PrintWriter(outFile);
    
    while(inputFile.hasNextLine())
    {
      String line = inputFile.nextLine();
      
      if(line.length() > 0)
      {
        outputFile.println(line);
      }
    }
    }  
    catch(FileNotFoundException ext )
    {
      System.err.println("Could not open file");
    }
    finally
    {
      if (inputFile != null)
        inputFile.close();
      if (outputFile != null)
        outputFile.close();
      
      System.out.println("File transfer complete.");
    }
  }
  
  ///////////////////////////////////////////////////////////////////
  //
  //  Write a method called "maxDistance" that accepts an integer array as its only
  //  argument. The method returns an integer that is the maximum difference among all
  //  elements of the argument array.
  //
  //  int[] values = { 3, 9, 4, 1, 5, 6 };
  //  int result;
  //
  //  result = maxDistance(values);   // result = 8
  
  public int maxDistance(int[] iNumbers)
  {
    int maxDifference = -1;
    
    Arrays.sort(iNumbers);
    maxDifference = Math.abs(iNumbers[0] - iNumbers[iNumbers.length - 1]);
    
    
    return maxDifference;
  }
}
